package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em -> {
            ParentEntity savedParent = parentEntityRepository.save(new ParentEntity("parent"));
            ChildEntity saveChild = childEntityRepository.save(new ChildEntity("Tom"));
            saveChild.setParentEntity(savedParent);
            parentId.setValue(savedParent.getId());
            childId.setValue(saveChild.getId());
        });
        assertEquals("Tom", childEntityRepository.findById(childId.getValue()).get().getName());
        assertEquals("parent", childEntityRepository.findById(parentId.getValue()).get().getParentEntity().getName());

        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em -> {
            ParentEntity savedParent = parentEntityRepository.save(new ParentEntity("parent"));
            ChildEntity saveChild = childEntityRepository.save(new ChildEntity("Tom"));
            saveChild.setParentEntity(savedParent);
            parentId.setValue(savedParent.getId());
            childId.setValue(saveChild.getId());
        });
        ChildEntity childEntity = childEntityRepository.findById(childId.getValue()).get();
        ParentEntity parentEntity = parentEntityRepository.findById(parentId.getValue()).get();
        childEntity.setParentEntity(null);
        assertNull(childEntityRepository.findById(parentId.getValue()).get().getParentEntity());

        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> parentId = new ClosureValue<>();
        ClosureValue<Long> childId = new ClosureValue<>();

        flushAndClear(em -> {
            ParentEntity savedParent = parentEntityRepository.save(new ParentEntity("parent"));
            ChildEntity saveChild = childEntityRepository.save(new ChildEntity("Tom"));
            saveChild.setParentEntity(savedParent);
            parentId.setValue(savedParent.getId());
            childId.setValue(saveChild.getId());
        });
        flush(em->{
            childEntityRepository.deleteById(childId.getValue());
            parentEntityRepository.deleteById(parentId.getValue());
        });
        assertFalse(childEntityRepository.findById(childId.getValue()).isPresent());
        assertFalse(parentEntityRepository.findById(parentId.getValue()).isPresent());

        // --end-->
    }
}
